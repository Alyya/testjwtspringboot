package com.example.demo.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Student;


public interface UserRepo extends JpaRepository<Student, Integer> {
	void deleteStudentByStudentId(Integer studentId);
	Optional<Student> findStudentByStudentId(Integer studentId);

}
