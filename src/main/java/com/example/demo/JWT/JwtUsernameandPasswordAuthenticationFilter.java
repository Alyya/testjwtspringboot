package com.example.demo.JWT;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class JwtUsernameandPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter{

	private final AuthenticationManager authenticationManager;
	
	
	public JwtUsernameandPasswordAuthenticationFilter(AuthenticationManager authenticationManager) {
		super();
		this.authenticationManager = authenticationManager;
	}


	public Authentication attemptAuthentication(HttpServletRequest request,
			                                    HttpServletResponse response) throws AuthenticationException {

		try{
		UsernameAndPasswordAuthenticationRequest authenticationRequest = new ObjectMapper()
		.readValue(request.getInputStream(), UsernameAndPasswordAuthenticationRequest.class);
	     Authentication authentication=new UsernamePasswordAuthenticationToken(
	    		 authenticationRequest.getUsername(),
	    		 authenticationRequest.getPassword()
	    		 );
	     
	     
	    Authentication authenticate = authenticationManager.authenticate(authentication);
		return authenticate;
		} catch (IOException e) {
          e.printStackTrace();
          throw new RuntimeException(e);
      }
  
		//return super.attemptAuthentication(request, response);
	
}
	 @Override
	    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
	                                            FilterChain chain, Authentication authResult) throws IOException, ServletException {
	String token= Jwts.builder()
	     .setSubject(authResult.getName())
	     .claim("authorities", authResult.getAuthorities())
	     .setIssuedAt(new Date())
	     .setExpiration(java.sql.Date.valueOf(LocalDate.now().plusWeeks(2)))
	     .signWith(Keys.hmacShaKeyFor("securesecuresecuresecuresecuresecure".getBytes()))
	     .compact();
         
	response.addHeader("Authorization", "Bearer "+ token);	 }
}














