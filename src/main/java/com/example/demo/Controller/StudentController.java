package com.example.demo.Controller;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Student;
import com.example.demo.Service.UserService;

@RestController
@RequestMapping("api/v1/students")

public class StudentController {
	public UserService service;
	/*
	private static final List<Student> STUDENTS =Arrays.asList(
			new Student (1,"ALYA"),
			new Student (2,"ADEM"),
			new Student (3,"MOEZ"),
			new Student (4,"Rania"));*/
	
	//@GetMapping(path ="{studentId}")
	/*public Student getStudent(@PathVariable("studentId") Integer studentId){
	return .stream()
			.filter(student -> studentId.equals(student.getStudentId()))
			.findFirst()
			.orElseThrow(() -> new IllegalStateException("Student"+ studentId+"does not exists"));*/
	
      @GetMapping(path ="/students")
	public ResponseEntity<List<Student>> getAllStudents() {
        List<Student> students = service.findAllStudents();
        return new ResponseEntity<>(students, HttpStatus.OK);
	
	}
	
      
      @GetMapping("/find/{studentId}")
      public ResponseEntity<Student> getStudentById (@PathVariable("studentId") Integer studentId) {
    	  Student student = service.findStudentById(studentId);
          return new ResponseEntity<>(student, HttpStatus.OK);
      }
}








