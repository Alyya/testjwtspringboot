package com.example.demo.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Student;
import com.example.demo.Service.UserService;

@RestController
@RequestMapping("management/api/v1/students")
public class StudentManagementController {
	public UserService service;

/*	private static final List<Student> STUDENTS =Arrays.asList(
			new Student (1,"ALYA"),
			new Student (2,"ADEM"),
			new Student (3,"MOEZ"),
			new Student (4,"Rania"));*/
	@GetMapping
	@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_ADMINTRAINEE')")
	public ResponseEntity<List<Student>> getAllStudents(){
		System.out.println("getAllStudents");
		 List<Student> students = service.findAllStudents();
	        return new ResponseEntity<>(students, HttpStatus.OK);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('student:write')")
	public void registredNewStudent(@RequestBody Student student) {
		System.out.println("student registred");
		System.out.println(student);
	}
	
	@DeleteMapping(path = "{studentId}")
	@PreAuthorize("hasAuthority('student:write')")
	public void deleteStudent(@PathVariable("studentId") Integer studentId) {
		System.out.println("delete student");
		System.out.println(studentId);
	}	
	
	@PutMapping(path = "{studentId}")	
	@PreAuthorize("hasAuthority('student:write')")

	public void updateStudent(@PathVariable("studentId") Integer studentId,@RequestBody Student student) {
		System.out.println(String.format("%s %s", studentId,student));

	}
	
}








