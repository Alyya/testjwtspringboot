package com.example.demo.Security;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.google.common.collect.Sets;

public enum ApplicationUserRole {

	STUDENT(Sets.newHashSet()),
    ADMIN(Sets.newHashSet(ApplicationUserPermission.COURSE_READ,ApplicationUserPermission.COURSE_WRITE,ApplicationUserPermission.STUDENT_WRITE,ApplicationUserPermission.STUDENT_READ)),
	ADMINTRINEE(Sets.newHashSet(ApplicationUserPermission.COURSE_READ,ApplicationUserPermission.STUDENT_READ));

	
	private final Set<ApplicationUserPermission> permissions;
	 ApplicationUserRole( Set<ApplicationUserPermission> permissions) {
		// TODO Auto-generated constructor stub
		this.permissions=permissions;
	}
	 
	 public Set<ApplicationUserPermission> getPermissions() {
		return permissions;
	}
	 public Set<GrantedAuthority> getGrantedAuthorities(){
         Set<GrantedAuthority> permissions =getPermissions().stream()
	    	.map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
	    	.collect(Collectors.toSet());
            permissions.add(new SimpleGrantedAuthority("ROLE_"+this.name()));
	    	return permissions;
	    }
}
