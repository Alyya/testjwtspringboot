package com.example.demo.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties.Admin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.example.demo.Controller.StudentController;
import com.example.demo.JWT.JwtUsernameandPasswordAuthenticationFilter;
import com.example.demo.Model.Student;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

private final PasswordEncoder passwordEncoder;
	
	 @Autowired
	 public ApplicationSecurityConfig(PasswordEncoder passwordEncoder) {
		 this.passwordEncoder=passwordEncoder;
	 }
	
	
	protected void configure(HttpSecurity http) throws Exception{
		 http
		 .csrf().disable()
		 .authorizeRequests()
		/* .antMatchers("/","/index","/css/*","/js/*").permitAll()*/
		// .antMatchers("/api/**").hasRole(ApplicationUserRole.STUDENT.name())
		// .antMatchers("management/api/**").hasRole(ApplicationUserRole.STUDENT.name())
		/* .antMatchers(HttpMethod.DELETE ,"/management/api/**").hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
		 .antMatchers(HttpMethod.POST ,"/management/api/**").hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
		 .antMatchers(HttpMethod.PUT ,"/management/api/**").hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
		 .antMatchers("/management/api/**").hasAnyRole(ApplicationUserRole.ADMIN.name(),ApplicationUserRole.ADMINTRINEE.name())
		 //many request are be authenticated*/
		/* .sessionManagement()
		          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		 .and()*/
		 
		 .anyRequest()
		 .permitAll();//jwt
		 /*.addFilter(new JwtUsernameandPasswordAuthenticationFilter(authenticationManager()))
		 .authorizeRequests()
		 .antMatchers("/","/index","/css/*","/js/*").permitAll()
		 .antMatchers("/api/**").hasRole(ApplicationUserRole.STUDENT.name())
		 .anyRequest()
		 .authenticated();*/
		/* .and()*/
		// .httpBasic();
		/* .formLogin()
	//t3ayet pagelogin static
		         .loginPage("/login")
		         .permitAll()	 
	         	 .defaultSuccessUrl("/courses",true)
	         	 .passwordParameter("password")
	         	 .usernameParameter("username")*/
		/* .and()
		 .rememberMe()
		 .and()
		 .logout()
		         .logoutUrl("/logout")
		         .logoutRequestMatcher(new AntPathRequestMatcher("/logout","GET"))
	        	 .clearAuthentication(true)
		         .invalidateHttpSession(true)
	        	 .deleteCookies("JSESSIONID","remember-me")
	        	 .logoutSuccessUrl("/login");//2weeks;*/
		 }
	
	@Bean
	protected UserDetailsService userDetailsService() {
		UserDetails alya =User.builder()
		.username("alya")
		.password(passwordEncoder.encode("1234"))
		//.roles(ApplicationUserRole.STUDENT.name())
		.authorities(ApplicationUserRole.STUDENT.getGrantedAuthorities())
		.build();
		
		
		UserDetails Rania = User.builder()
		.username("Rania").password(passwordEncoder.encode("1234"))
//				.roles(ApplicationUserRole.ADMIN.name())
		.authorities(ApplicationUserRole.ADMIN.getGrantedAuthorities())
		
				.build();
		
		UserDetails tomUser = User.builder()
				.username("tom").password(passwordEncoder.encode("1234"))
			//			.roles(ApplicationUserRole.ADMINTRINEE.name())
				.authorities(ApplicationUserRole.ADMINTRINEE.getGrantedAuthorities())
				.build();
		return new InMemoryUserDetailsManager
				(alya,Rania,tomUser);				
	}
	
	}

















