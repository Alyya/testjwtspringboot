package com.example.demo.Service;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Exception.UserNotFoundException;
import com.example.demo.Model.Student;
import com.example.demo.Repository.UserRepo;


@Service
@Transactional
public class UserService {
	private final UserRepo repo;

	public UserService(UserRepo repo) {
		super();
		this.repo = repo;
	}
	
	 public Student addStudent(Student product) {
	        product.setPassword(UUID.randomUUID().toString());
	        return repo.save(product);
	    }
	 
	 public List<Student> findAllStudents (){
		 return repo.findAll();	 }
	 
	 
	  public Student findStudentById(Integer studentID) {
	        return repo.findStudentByStudentId(studentID)
	                .orElseThrow(() -> new UserNotFoundException("product by id " + studentID + " was not found"));
	    }


}
