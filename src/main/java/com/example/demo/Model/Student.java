package com.example.demo.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Student {

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + "]";
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private  Integer studentId;
    private  String studentName;
    private String password;
    private String role;
	public Integer getStudentId() {
		return studentId;
	}
		
	
	public Student() {
		super();
	}

	public Student(Integer studentId, String studentName, String password, String role) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.password = password;
		this.role = role;
	}






	public Student(String studentName, String password, String role) {
		super();
		this.studentName = studentName;
		this.password = password;
		this.role = role;
	}






	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
}